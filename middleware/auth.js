const jwt = require('jsonwebtoken');

module.exports = function (req, res, next) {
    // Leer el token del header
    const token = req.header('x-auth-token');

    // Revisar si no hay token
    if(!token) {
        res.status(401).json({ msg: 'No hay token, permiso no valido.' });
    }

    // Validar el token
    try {
       const cifrado = jwt.verify(token, process.env.SECRET_KEY);
       req.usuario = cifrado.usuario; // este usuario viene del payload cuando se crea el usuario
       next();
    } catch (error) {
        res.status(401).json({ msg: 'Token no valido.' });
    }
};
