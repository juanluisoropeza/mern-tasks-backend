const mongoose = require('mongoose');
require('dotenv').config({ path: 'variables.env' });

const conectarDB = async () => {
    try {
        await mongoose.connect(process.env.DB_MONGO, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
            useCreateIndex: true
        });
        console.log("DB Conectada")
    } catch (error) {
        console.log(error);
        proccess.exit(1); // si hay un error en la app detiene la app
    }
};

module.exports = conectarDB;
