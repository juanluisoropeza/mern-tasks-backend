const express = require('express');
const conectarDB = require('./config/db');

// crear el servidor
const app = express();

// conectar a la base de datos
conectarDB();

// habilitar express.json para leer los datos que el usuario envie
app.use(express.json({ extended: true }));

// puerto de la app
const PORT = process.env.PORT || 4000;

// importar rutas
app.use('/api/usuarios',  require('./routes/usuarios'));
app.use('/api/auth',  require('./routes/auth'));
app.use('/api/proyectos',  require('./routes/proyectos'));
app.use('/api/tareas',  require('./routes/tareas'));

// definir pagina principal
app.get('/', (req, res) => {
    res.send("Hola mundo");
});

app.listen(PORT, () => {
    console.log(`El servidor esta funcionando en el puerto ${PORT}`)
});