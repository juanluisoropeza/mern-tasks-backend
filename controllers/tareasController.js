const { validationResult } = require('express-validator');

const Tareas = require('../models/Tareas');
const Proyectos = require('../models/Proyectos');

// Crea una nueva tarea
exports.crearTarea = async (req, res) => {

    // Revisar si hay errores
    const errors = validationResult(req);
    if(!errors.isEmpty()) {
        return res.status(400).json({ errores: errors.array() });
    }

    try {

        // Extraer el proyecto y comprobar que existe
        const { proyecto } = req.body;

        const existeProyecto = await Proyectos.findById(proyecto);
        if(!existeProyecto) {
            return res.status(404).json({ msg: 'Proyecto no encontrado.' });
        }

        // Revisar si el proyecto actual pertenece al usuario autenticado
        if(existeProyecto.creador.toString() !== req.usuario.id) {
            return res.status(401).json({ msg: 'No autorizado.' });
        }

        // Creamos la tareea
        const tarea = new Tareas(req.body);
        await tarea.save();
        res.status(201).json(tarea);

    } catch (error) {
        console.log(error);
        res.status(500).send('Error al agregar una tarea.');
    }

};

// Obtiene las tareas por proyecto
exports.obtenerTareas = async (req, res) => {
    try {

        // Extraer el proyecto y comprobar que existe
        const { proyecto } = req.body;

        const existeProyecto = await Proyectos.findById(proyecto);
        if(!existeProyecto) {
            return res.status(404).json({ msg: 'Proyecto no encontrado.' });
        }

        // Revisar si el proyecto actual pertenece al usuario autenticado
        if(existeProyecto.creador.toString() !== req.usuario.id) {
            return res.status(401).json({ msg: 'No autorizado.' });
        }

        // Obtener las tareas por proyecto
        const tareas = await Tareas.find({ proyecto });
        res.json({ tareas });

    } catch (error) {
        console.log(error);
        res.status(500).send('Error al obtener tareas.');
    }
};

// Actualizar una tarea
exports.actualizarTareas = async (req, res) => {
    try {

        // Extraer el proyecto y comprobar que existe
        const { proyecto, nombre, estado } = req.body;

        // Revisar si la tarea existe o no
        let tarea = await Tareas.findById(req.params.id);
        if(!tarea) {
            return res.status(404).json({ msg: 'La tarea no existe.' });
        }

        // Extraer el proyecto
        const existeProyecto = await Proyectos.findById(proyecto);

        // Revisar si el proyecto actual pertenece al usuario autenticado
        if(existeProyecto.creador.toString() !== req.usuario.id) {
            return res.status(401).json({ msg: 'No autorizado.' });
        }

        // Crear un objeto con la nueva informacion
        const nuevaTarea = {};

        if(nombre) {
            nuevaTarea.nombre = nombre;
        }

        if(estado) {
            nuevaTarea.estado = estado;
        }

        // Guardar la tarea
        tarea = await Tareas.findOneAndUpdate(
            { _id: req.params.id }, 
            nuevaTarea,
            { new: true }
        );

        res.json({ tarea });

    } catch (error) {
        console.log(error);
        res.status(500).send('Error al actualizar tarea.');
    }
};

// Eliminar Tarea
exports.eliminarTareas = async (req, res) => {
    try {

        // Extraer el proyecto y comprobar que existe
        const { proyecto } = req.body;

        // Revisar si la tarea existe o no
        let tarea = await Tareas.findById(req.params.id);
        if(!tarea) {
            return res.status(404).json({ msg: 'La tarea no existe.' });
        }

        // Extraer el proyecto
        const existeProyecto = await Proyectos.findById(proyecto);

        // Revisar si el proyecto actual pertenece al usuario autenticado
        if(existeProyecto.creador.toString() !== req.usuario.id) {
            return res.status(401).json({ msg: 'No autorizado.' });
        }

        // Eliminar
        await Tareas.findOneAndRemove({ _id: req.params.id });

        res.json({ msg: 'Tarea eliminada con exito.' });

    } catch (error) {
        console.log(error);
        res.status(500).send('Error al eliminar tarea.');
    }
}
