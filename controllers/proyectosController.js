const Proyectos = require('../models/Proyectos');
const { validationResult } = require('express-validator');

exports.crearProyecto = async (req, res) => {

    // Revisar si hay errores
    const errors = validationResult(req);
    if(!errors.isEmpty()) {
        return res.status(400).json({ errores: errors.array() });
    }

    try {
        // Crear un nuevo proyecto
        const proyecto = new Proyectos(req.body);

        // Guardar el creador del proyecto via JWT
        proyecto.creador = req.usuario.id;

        proyecto.save();
        res.status(201).json(proyecto);
    } catch (error) {
        console.log(error);
        res.status(500).send('Error al agregar un proyecto.');
    }
};

// Obtiene todos los proyectos del usuario actual
exports.obtenerProyectos = async (req, res) => {
    try {
        const proyectos = await Proyectos.find({ creador: req.usuario.id }).sort({ creado: -1});
        res.status(200).json({ proyectos });
    } catch (error) {
        console.log(error);
        res.status(500).send('Error al obtener los proyectos.');
    }
};

// Actualizar un proyecto
exports.actualizarProyecto = async (req, res) => {

    // Revisar si hay errores
    const errors = validationResult(req);
    if(!errors.isEmpty()) {
        return res.status(400).json({ errores: errors.array() });
    }
    
    // extraer la info del proyecto
    const { nombre } = req.body;
    const nuevoProyecto = {};
    if(nombre) {
        nuevoProyecto.nombre = nombre;
    }

    try {
        // Revisar el ID
        let proyecto = await Proyectos.findById(req.params.id);

        // si el proyecto existe o no
        if(!proyecto) {
            return res.status(404).json({ msg: 'Proyecto no encontrado.' });
        }

        // verificar el creador del proyecto
        if(proyecto.creador.toString() !== req.usuario.id) {
            return res.status(401).json({ msg: 'No autorizado.' });
        }

        // actualizar
        proyecto = await Proyectos.findOneAndUpdate({ _id: req.params.id }, { $set: nuevoProyecto }, { new: true });

        res.status(200).json({ proyecto });

    } catch (error) {
        console.log(error);
        res.status(500).send('Error al actualizar un proyecto.');
    }
};

// Elimina un proyecto por su ID
exports.eliminarProyecto = async (req, res) => {
    try {

        // Revisar el ID
        let proyecto = await Proyectos.findById(req.params.id);
    
        // si el proyecto existe o no
        if(!proyecto) {
            return res.status(404).json({ msg: 'Proyecto no encontrado.' });
        }
    
        // verificar el creador del proyecto
        if(proyecto.creador.toString() !== req.usuario.id) {
            return res.status(401).json({ msg: 'No autorizado.' });
        }

        // Eliminar proyecto
        await Proyectos.findOneAndRemove({ _id: req.params.id });
        res.status(200).json({ msg: 'Proyecto eliminado.'});
        
    } catch (error) {
        console.log(error);
        res.status(500).send('Error al eliminar un proyecto.');
    }
};
