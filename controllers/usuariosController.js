const express = require('express');
const bcryptjs = require('bcryptjs');
const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');

const Usuario = require('../models/Usuarios');

exports.crearUsuario = async (req, res) => {

    // revisar si hay errores
    const errors = validationResult(req);
    if(!errors.isEmpty()) {
        return res.status(400).json({ errores: errors.array() });
    }

    // extraer email y password
    const { email, password } = req.body;

    try {
        // Revisar que el email del usuario sea unico
        let usuario = await Usuario.findOne({ email });
        if(usuario) {
            return res.status(400).json({ msg: 'El usuario ya existe'});
        }

        // crea el nuevo usuario
        usuario = new Usuario(req.body);

        // hashear el password
        const salt = await bcryptjs.genSalt(12);
        usuario.password = await bcryptjs.hash(password, salt);

        // guardar usuario
        await usuario.save();

        // Crear y firmar el JWT
        const payload = {
            usuario: {
                id: usuario.id
            }
        };

        // firmar el jwt
        jwt.sign(payload, process.env.SECRET_KEY, {
            expiresIn: 3600 // 1 hora
        }, (error, token) => {
            if(error) throw error;
             
            // mensaje de confirmacion
            res.json({ token });
        });

        // mensaje de confirmacion
        // res.status(200).json({ msg: 'Usuario creado correctamente.' });

    } catch (error) {
        console.log(error);
        res.status(400).send('Hubo un error.');
    }
};