const express = require('express');
const router = express.Router();
const { check } = require('express-validator');

const proyectosController = require('../controllers/proyectosController');
const auth = require('../middleware/auth');

// Crea proyectos
// api/proyectos
router.post('/',
    auth,
    [
        check('nombre', 'El nombre del proyecto es obligatorio.').not().isEmpty()
    ],
    proyectosController.crearProyecto
);

// Listar proyectos
// api/proyectos
router.get('/',
    auth,
    proyectosController.obtenerProyectos
);

// Actualizar proyecto via ID
// api/proyectos
router.put('/:id',
    auth,
    [
        check('nombre', 'El nombre del proyecto es obligatorio.').not().isEmpty()
    ],
    proyectosController.actualizarProyecto
);

// Eliminar proyecto via ID
// api/proyectos
router.delete('/:id',
    auth,
    proyectosController.eliminarProyecto
);

module.exports = router;
