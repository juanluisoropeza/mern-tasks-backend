const express = require('express');
const { check } = require('express-validator');
const router = express.Router();

const tareasController = require('../controllers/tareasController');
const auth = require('../middleware/auth');

// Crea Tarea
// api/tareas
router.post('/',
    auth,
    [
        check('nombre', 'El nombre de la tarea es obligatorio.').not().isEmpty(),
        check('proyecto', 'El proyecto es obligatorio.').not().isEmpty()
    ],
    tareasController.crearTarea
);

// Obtener las tareas por proyecto
router.get('/',
    auth,
    tareasController.obtenerTareas
);

// Actualizar Tarea
router.put('/:id',
    auth,
    tareasController.actualizarTareas
);

// Eliminar tarea
router.delete('/:id',
    auth,
    tareasController.eliminarTareas
);


module.exports = router;