const mongoose = require('mongoose');

const TareasSchema = mongoose.Schema({
    nombre: { type: String, required: true, trim: true },
    estado: { type: Boolean, default: false },
    creado: { type: Date, default: Date.now() },
    proyecto: { 
        type: mongoose.Schema.Types.ObjectId, // es el nombre del modelo que se le dio en el module.exports al Usuarios schema, module.exports = mongoose.model('Usuario', UsuariosSchema);
        ref: 'Proyectos', // nombre del modelo  al que se hace referencia
    }
});

module.exports = mongoose.model('Tareas', TareasSchema);
