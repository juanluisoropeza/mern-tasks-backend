const mongoose = require('mongoose');

const ProyectoSchema = mongoose.Schema({
    nombre: { type: String, required: true, trim: true },
    creador: { 
        type: mongoose.Schema.Types.ObjectId, // como type se pasa el id del usuario como referencia
        ref: 'Usuario', // es el nombre del modelo que se le dio en el module.exports al Usuarios schema, module.exports = mongoose.model('Usuario', UsuariosSchema);
    }, 
    creado: { type: Date, default: Date.now() },
});

module.exports = mongoose.model('Proyectos', ProyectoSchema);
